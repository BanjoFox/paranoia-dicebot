module.exports = {
	name: 'node',
	description: 'Get the players NODE, and roll it!',
	execute(message, args) {
        putAdvice()
        + message.reply("Rolled: " + rollNODE());
        + message.channel.send("Computer Rolled: " + rollCOMPUTER())
        + countSuccess();

        const amount = parseInt(args[0]) + 1;

        // Check argument is a number
		if (isNaN(amount)) {
			return message.reply('that doesn\'t seem to be a valid number.');
		}

        // Computer gives helpful advice
        function putAdvice() {
        var advice ="";
        if (args < 0) {
                advice = message.channel.send("Citizen! Please report to R&D for Mandatory UP-Grades");
            } else if (args === 0) {
                advice = message.channel.send("You seem to require assistance.");
            } else {
                advice = message.channel.send("Great Job! All of Alpha Complex can count on you.");
            }
            return advice;
        }

        // Roll players NODE
        function rollNODE() {
            var counter = Math.abs(args);
            var playerDice = [];
            while (counter > 0) {
                var dice = Math.floor((Math.random() * 6) + 1);
                playerDice.push(dice);
                counter--;
            }
            return playerDice;
        }

        function rollCOMPUTER() {
            var computerDice = [Math.floor((Math.random() * 6) + 1)];
            if (computerDice === 6) {
                computerDice = "COMPUTER";
            }
            return computerDice;
        }

        function countSuccess() {

            totalDice = rollNODE().push(rollCOMPUTER());
            var num_success = totalDice.map((i) => i >= 5 ? 1 : 0).reduce((total, j) => total + j);
            var num_not_success = totalDice.map((i) => i < 5 ? 1 : 0).reduce((total, j) => total + j);
            //    var success_pl = "es" if num_success > 1 else ""
            //    var not_success_pl = "es" if num_not_success > 1 else ""

            var outcome = ""
            if (args > 0) {
                outcome = message.channel.send("Successes: " + (num_not_success - num_success));
            } else {
                outcome = message.channel.send("Successes: " + num_success);
            }
            return outcome;
        }

	},
};
