# Paranoia Dicebot

This is a Discord bot programmed to roll dice for the Paranoia RPG (Red Clearance Edition)
Use of this bot without attribution is treason.


```
#-
# Paranoia Dice Rules
#
# NODE - Number of Dice (Signed Int)
# Computer Dice Face Values- 1,2,3,4,5,Computer
# Success - Any d6 outcome that is greater than or equal to 5
# Negative NODE - Any d6 outcome which is >5 subtracts from number of total Successes
#
```
