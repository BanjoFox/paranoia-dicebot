import random
import discord

from discord.ext import commands

#----
tokenfile = 'token.txt'
with open(tokenfile) as tf:
    line = tf.readline()
    TOKEN = line.rstrip()
#----

bot = commands.Bot(command_prefix='$')

@bot.command(name='99', help='Responds with a random quote from Brooklyn 99')
async def nine_nine(self, ctx, member : discord.Member):
    brooklyn_99_quotes = [
        'I\'m the human form of the 💯 emoji.',
        'Bingpot!',
        (
            'Cool. Cool cool cool cool cool cool cool, '
            'no doubt no doubt no doubt no doubt.'
        ),
    ]

    response = random.choice(brooklyn_99_quotes)
    await ctx.send("{} your quote is: {}".format(member, response))

bot.run( TOKEN)
