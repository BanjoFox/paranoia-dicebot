#-
# So theres this thing right.
# It rolls dice for people.
# People like being able to roll dice
# Because dice are good.
# Friend Computer is also good!
# We LOVE Friend Computer!
# This is one of Friend Computer's MOST LOYAL Bots.
# Let the bot roll dice for you.
# Or else Friend Computer will wonder why you are a Traitor.
# You are not a Traitor...
# Right?
#
# +-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+-+
# |S|E|C|U|R|I|T|Y| |C|L|E|A|R|A|N|C|E|
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |U|L|T|R|A|V|I|O|L|E|T|
# +-+-+-+-+-+-+-+-+-+-+-+

#-
# Import some stuff
from random import randint
import discord
from discord.ext import commands

#-
# Let's do 'BOT Stuff' ;)
# BOT terminal prompt
bot = commands.Bot(command_prefix='$')
NODE = 0

# -
# Beyond this line things get dicey ↴
# -------------------------------------
@bot.command(name='node', help='Responds with a random quote from Brooklyn 99')
async def bot_stuff(ctx, NODE):

	author = ctx.message.author

	# Computer says...
	NODE = int(NODE)
	if NODE > 0:
		com_say = "Excellent work Citizen, your NODE means that you cannot reasonably fail."
	elif NODE == 0:
		com_say = "Would you like some help Citizen?"
	else:
		com_say = "Citizen, Please report to R&D for a MANDATORY UP-GRADE to your Cerebral CoreTech."

	# Roll NODE
	roll = []
	NODE_count = abs ( NODE )

	while NODE_count > 0 :
		roll.append( int( randint(1, 6) ))
		NODE_count -= 1

	# Roll COMPUTER
	computer_dice = randint(1, 6)
	if computer_dice > 5:
		computer_dice = "COMPUTER"

	# Count Successes...or Not
	num_not_success = sum(map(lambda b: 1 if b < 5 else 0, roll))
	num_success = sum(map(lambda b: 1 if b >= 5 else 0, roll))

	#-
	# CONSTRUCTING PYLONS
	#
	dice_out = "{} rolls {} | ".format(author, roll)
	dice_com = "Computer Rolled: {}".format(computer_dice)
	success_count = "{} successes, {} ...not successes.".format(num_success, num_not_success)

	#-
	# Make with the outputs!
	#
	await ctx.send(com_say)
	await ctx.send(dice_out + dice_com)
	await ctx.send(success_count)

@bot.event
async def on_ready():
    print('{0.user}, has entered the briefing area.'.format(bot))

bot.run('TOKEN')
