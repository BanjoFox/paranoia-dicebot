#-
# So theres this thing right.
# It rolls dice for people.
# People like being able to roll dice
# Because dice are good.
# Friend Computer is also good!
# We LOVE Friend Computer!
# This is one of Friend Computer's MOST LOYAL Bots.
# Let the bot roll dice for you.
# Or else Friend Computer will wonder why you are a Traitor.
# You are not a Traitor...
# Right?
#
# +-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+-+
# |S|E|C|U|R|I|T|Y| |C|L|E|A|R|A|N|C|E|
# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
# |U|L|T|R|A|V|I|O|L|E|T|
# +-+-+-+-+-+-+-+-+-+-+-+
#

#-
# Built from ELLIE's original Python CLI, and the help of
# https://discordpy.readthedocs.io/en/latest/quickstart.html
#

#-
# Import some stuff
#
from random import randint
import discord # Imported from https://github.com/Rapptz/discord.py
#import asyncio
from discord.ext import commands
import logging

#-
# Get the API Token, then make it so.
# BECAUSE THERE IS ABSOLUTELY NO OTHER
# /POSSIBLE/ WAY TO DO THIS!!!!!!!!
tokenfile = 'token.txt'
with open(tokenfile) as tf:
    line = tf.readline()
    TOKEN = line.rstrip()


#-
# Let's do 'BOT Stuff' ;)
bot = discord.Client()
bot = commands.Bot(command_prefix='$')
NODE = 0

# POST Beeps
@bot.event
async def on_ready():
    print('{0.user}, has entered the briefing area'.format(bot))

# -
# Beyond this line things get dicey ↴
# -------------------------------------

#-
# Get the players NODE
#
def computer_says():
    if NODE > 0:
        com_say = "Excellent work Citizen, your NODE means that you cannot reasonably fail."
    elif NODE == 0:
        com_say = "Would you like some help Citizen?"
    else:
        com_say = "Citizen, Please report to R&D for a MANDATORY UP-GRADE to your Cerebral CoreTech."
    return com_say

#-
# Get absolute value of NODE, and roll dice :)
#
def roll_NODE():
    dice_result = []
    NODE_count = abs ( NODE )

    while NODE_count > 0 :
        dice_result.append( int( randint(1, 6) ))
        NODE_count -= 1
    return dice_result

#-
# Lets get Friend Computer involved! :D
#
def roll_Computer():
    computer_dice = randint(1, 6)
    if computer_dice > 5:
        computer_dice = "COMPUTER"
    return computer_dice

#-
# OUTPUT
#
@bot.command(name='node', help='Rolls dice. Supply an integer (positive or negative).')
async def bot_roller(ctx, NODE, member : discord.Member):
    #author = ctx.message.author
    #author = ctx.message.mentions[0].id

    try:
        NODE = int(NODE)
    except:
        await ctx.send("{} That isn't a number.".format(member))
        return
    #-
    # Collect all the variables!
    node_cmt = computer_says()
    dice_result = roll_NODE(NODE)
    dice_computer = roll_Computer()
    num_not_success = sum(map(lambda b: 1 if b < 5 else 0, dice_result))
    num_success = sum(map(lambda b: 1 if b >= 5 else 0, dice_result))

    #-
    # Textual Feelings
    dice_out = "{} rolls {} | ".format(member, roll_NODE())
    dice_com = "Computer Rolled: {}".format(roll_Computer())
    success_pl = "es" if num_success > 1 else ""
    not_success_pl = "es" if num_not_success > 1 else ""
    success_count = "@{} success{}, {} ...not success{}.".format(num_success, success_pl, num_not_success, not_success_pl)


    #-
    # Make with the outputs!
    #
    await ctx.send(node_cmt)
    await ctx.send(dice_out + dice_com)
    await ctx.send(success_count)

#-
# Step 1) Chop wood
# Step 2) Get log(s)
# Step 3) It's better than bad its WOOD!
#

logger = logging.getLogger('discord')
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

#-
# Begin boot cycle
#

print( "Charging powerpack" )

bot.run( TOKEN )


print( "Powerpack 10000% FULL" )

#-
# Allegedly we can do some stuff to generate this registration URL dynamically,
# but I have no idea how, or why we would want to do that...
# ...allegedly
#
# URL: https://discordapp.com/api/oauth2/authorize?client_id=634116482749431818&permissions=465984&scope=bot
#
