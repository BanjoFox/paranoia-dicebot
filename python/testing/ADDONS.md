
```
#-
#This event handles an error event from the command and sends an informative error message 
#back to the original Context of the invoked Command.
@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.errors.CheckFailure):
        await ctx.send('You do not have the correct role for this command.')
```

**Also Try**

`ctx.message.author.mention`